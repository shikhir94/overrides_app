var _WM_APP_PROPERTIES = {
  "activeTheme" : "default",
  "defaultLanguage" : "en",
  "displayName" : "overrides",
  "homePage" : "Main",
  "name" : "overrides",
  "platformType" : "WEB",
  "securityEnabled" : "true",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};