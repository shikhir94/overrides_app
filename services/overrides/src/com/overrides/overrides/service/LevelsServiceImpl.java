/*Copyright (c) 2016-2017 All Rights Reserved.
 This software is the confidential and proprietary information of cruiseplanners.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered.*/
package com.overrides.overrides.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.List;
import java.util.Map;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.data.util.DaoUtils;
import com.wavemaker.runtime.file.model.Downloadable;

import com.overrides.overrides.Incentives;
import com.overrides.overrides.Levels;


/**
 * ServiceImpl object for domain model class Levels.
 *
 * @see Levels
 */
@Service("overrides.LevelsService")
@Validated
public class LevelsServiceImpl implements LevelsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LevelsServiceImpl.class);

    @Lazy
    @Autowired
	@Qualifier("overrides.IncentivesService")
	private IncentivesService incentivesService;

    @Autowired
    @Qualifier("overrides.LevelsDao")
    private WMGenericDao<Levels, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Levels, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "overridesTransactionManager")
    @Override
	public Levels create(Levels levels) {
        LOGGER.debug("Creating a new Levels with information: {}", levels);

        List<Incentives> incentiveses = levels.getIncentiveses();
        if(incentiveses != null && Hibernate.isInitialized(incentiveses)) {
            incentiveses.forEach(_incentives -> _incentives.setLevels(levels));
        }

        Levels levelsCreated = this.wmGenericDao.create(levels);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(levelsCreated);
    }

	@Transactional(readOnly = true, value = "overridesTransactionManager")
	@Override
	public Levels getById(Integer levelsId) {
        LOGGER.debug("Finding Levels by id: {}", levelsId);
        return this.wmGenericDao.findById(levelsId);
    }

    @Transactional(readOnly = true, value = "overridesTransactionManager")
	@Override
	public Levels findById(Integer levelsId) {
        LOGGER.debug("Finding Levels by id: {}", levelsId);
        try {
            return this.wmGenericDao.findById(levelsId);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Levels found with id: {}", levelsId, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "overridesTransactionManager")
	@Override
	public Levels update(Levels levels) {
        LOGGER.debug("Updating Levels with information: {}", levels);

        List<Incentives> incentiveses = levels.getIncentiveses();
        if(incentiveses != null && Hibernate.isInitialized(incentiveses)) {
            incentiveses.forEach(_incentives -> _incentives.setLevels(levels));
        }

        this.wmGenericDao.update(levels);
        this.wmGenericDao.refresh(levels);

        // Deleting children which are not present in the list.
        if(incentiveses != null && Hibernate.isInitialized(incentiveses) && !incentiveses.isEmpty()) {
            List<Incentives> _remainingChildren = wmGenericDao.execute(
                session -> DaoUtils.findAllRemainingChildren(session, Incentives.class,
                        new DaoUtils.ChildrenFilter<>("levels", levels, incentiveses)));
            LOGGER.debug("Found {} detached children, deleting", _remainingChildren.size());
            _remainingChildren.forEach(_incentives -> incentivesService.delete(_incentives));
            levels.setIncentiveses(incentiveses);
        }

        return levels;
    }

    @Transactional(value = "overridesTransactionManager")
	@Override
	public Levels delete(Integer levelsId) {
        LOGGER.debug("Deleting Levels with id: {}", levelsId);
        Levels deleted = this.wmGenericDao.findById(levelsId);
        if (deleted == null) {
            LOGGER.debug("No Levels found with id: {}", levelsId);
            throw new EntityNotFoundException(String.valueOf(levelsId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "overridesTransactionManager")
	@Override
	public void delete(Levels levels) {
        LOGGER.debug("Deleting Levels with {}", levels);
        this.wmGenericDao.delete(levels);
    }

	@Transactional(readOnly = true, value = "overridesTransactionManager")
	@Override
	public Page<Levels> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Levels");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "overridesTransactionManager")
    @Override
    public Page<Levels> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Levels");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "overridesTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service overrides for table Levels to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "overridesTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "overridesTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "overridesTransactionManager")
    @Override
    public Page<Incentives> findAssociatedIncentiveses(Integer levelId, Pageable pageable) {
        LOGGER.debug("Fetching all associated incentiveses");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("levels.levelId = '" + levelId + "'");

        return incentivesService.findAll(queryBuilder.toString(), pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service IncentivesService instance
	 */
	protected void setIncentivesService(IncentivesService service) {
        this.incentivesService = service;
    }

}

