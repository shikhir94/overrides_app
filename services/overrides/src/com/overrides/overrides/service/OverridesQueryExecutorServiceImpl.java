/*Copyright (c) 2016-2017 All Rights Reserved.
 This software is the confidential and proprietary information of cruiseplanners.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered.*/

package com.overrides.overrides.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.wavemaker.runtime.data.dao.query.WMQueryExecutor;

@Service
public class OverridesQueryExecutorServiceImpl implements OverridesQueryExecutorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(OverridesQueryExecutorServiceImpl.class);

    @Autowired
    @Qualifier("overridesWMQueryExecutor")
    private WMQueryExecutor queryExecutor;


}


