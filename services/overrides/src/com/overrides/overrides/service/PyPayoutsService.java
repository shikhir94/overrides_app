/*Copyright (c) 2016-2017 All Rights Reserved.
 This software is the confidential and proprietary information of cruiseplanners.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered.*/
package com.overrides.overrides.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.overrides.overrides.PyPayouts;

/**
 * Service object for domain model class {@link PyPayouts}.
 */
public interface PyPayoutsService {

    /**
     * Creates a new PyPayouts. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on PyPayouts if any.
     *
     * @param pyPayouts Details of the PyPayouts to be created; value cannot be null.
     * @return The newly created PyPayouts.
     */
	PyPayouts create(@Valid PyPayouts pyPayouts);


	/**
	 * Returns PyPayouts by given id if exists.
	 *
	 * @param pypayoutsId The id of the PyPayouts to get; value cannot be null.
	 * @return PyPayouts associated with the given pypayoutsId.
     * @throws EntityNotFoundException If no PyPayouts is found.
	 */
	PyPayouts getById(Long pypayoutsId);

    /**
	 * Find and return the PyPayouts by given id if exists, returns null otherwise.
	 *
	 * @param pypayoutsId The id of the PyPayouts to get; value cannot be null.
	 * @return PyPayouts associated with the given pypayoutsId.
	 */
	PyPayouts findById(Long pypayoutsId);


	/**
	 * Updates the details of an existing PyPayouts. It replaces all fields of the existing PyPayouts with the given pyPayouts.
	 *
     * This method overrides the input field values using Server side or database managed properties defined on PyPayouts if any.
     *
	 * @param pyPayouts The details of the PyPayouts to be updated; value cannot be null.
	 * @return The updated PyPayouts.
	 * @throws EntityNotFoundException if no PyPayouts is found with given input.
	 */
	PyPayouts update(@Valid PyPayouts pyPayouts);

    /**
	 * Deletes an existing PyPayouts with the given id.
	 *
	 * @param pypayoutsId The id of the PyPayouts to be deleted; value cannot be null.
	 * @return The deleted PyPayouts.
	 * @throws EntityNotFoundException if no PyPayouts found with the given id.
	 */
	PyPayouts delete(Long pypayoutsId);

    /**
	 * Deletes an existing PyPayouts with the given object.
	 *
	 * @param pyPayouts The instance of the PyPayouts to be deleted; value cannot be null.
	 */
	void delete(PyPayouts pyPayouts);

	/**
	 * Find all PyPayouts matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
	 *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
	 *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching PyPayouts.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
	 */
    @Deprecated
	Page<PyPayouts> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
	 * Find all PyPayouts matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching PyPayouts.
     *
     * @see Pageable
     * @see Page
	 */
    Page<PyPayouts> findAll(String query, Pageable pageable);

    /**
	 * Exports all PyPayouts matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
	 *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
	 */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

	/**
	 * Retrieve the count of the PyPayouts in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
	 * @return The count of the PyPayouts.
	 */
	long count(String query);

	/**
	 * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
	 * @return Paginated data with included fields.

     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
	Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);


}

