/*Copyright (c) 2016-2017 All Rights Reserved.
 This software is the confidential and proprietary information of cruiseplanners.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered.*/
package com.overrides.overrides.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.overrides.overrides.Dates;


/**
 * ServiceImpl object for domain model class Dates.
 *
 * @see Dates
 */
@Service("overrides.DatesService")
@Validated
public class DatesServiceImpl implements DatesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatesServiceImpl.class);


    @Autowired
    @Qualifier("overrides.DatesDao")
    private WMGenericDao<Dates, Long> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Dates, Long> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "overridesTransactionManager")
    @Override
	public Dates create(Dates dates) {
        LOGGER.debug("Creating a new Dates with information: {}", dates);

        Dates datesCreated = this.wmGenericDao.create(dates);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(datesCreated);
    }

	@Transactional(readOnly = true, value = "overridesTransactionManager")
	@Override
	public Dates getById(Long datesId) {
        LOGGER.debug("Finding Dates by id: {}", datesId);
        return this.wmGenericDao.findById(datesId);
    }

    @Transactional(readOnly = true, value = "overridesTransactionManager")
	@Override
	public Dates findById(Long datesId) {
        LOGGER.debug("Finding Dates by id: {}", datesId);
        try {
            return this.wmGenericDao.findById(datesId);
        } catch(EntityNotFoundException ex) {
            LOGGER.debug("No Dates found with id: {}", datesId, ex);
            return null;
        }
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "overridesTransactionManager")
	@Override
	public Dates update(Dates dates) {
        LOGGER.debug("Updating Dates with information: {}", dates);

        this.wmGenericDao.update(dates);
        this.wmGenericDao.refresh(dates);

        return dates;
    }

    @Transactional(value = "overridesTransactionManager")
	@Override
	public Dates delete(Long datesId) {
        LOGGER.debug("Deleting Dates with id: {}", datesId);
        Dates deleted = this.wmGenericDao.findById(datesId);
        if (deleted == null) {
            LOGGER.debug("No Dates found with id: {}", datesId);
            throw new EntityNotFoundException(String.valueOf(datesId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "overridesTransactionManager")
	@Override
	public void delete(Dates dates) {
        LOGGER.debug("Deleting Dates with {}", dates);
        this.wmGenericDao.delete(dates);
    }

	@Transactional(readOnly = true, value = "overridesTransactionManager")
	@Override
	public Page<Dates> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Dates");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "overridesTransactionManager")
    @Override
    public Page<Dates> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Dates");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "overridesTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service overrides for table Dates to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "overridesTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "overridesTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}

