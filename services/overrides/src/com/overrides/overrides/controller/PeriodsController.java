/*Copyright (c) 2016-2017 All Rights Reserved.
 This software is the confidential and proprietary information of cruiseplanners.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered.*/
package com.overrides.overrides.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.overrides.overrides.Incentives;
import com.overrides.overrides.Periods;
import com.overrides.overrides.PyPayouts;
import com.overrides.overrides.PyPerformances;
import com.overrides.overrides.service.PeriodsService;


/**
 * Controller object for domain model class Periods.
 * @see Periods
 */
@RestController("overrides.PeriodsController")
@Api(value = "PeriodsController", description = "Exposes APIs to work with Periods resource.")
@RequestMapping("/overrides/Periods")
public class PeriodsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PeriodsController.class);

    @Autowired
	@Qualifier("overrides.PeriodsService")
	private PeriodsService periodsService;

	@ApiOperation(value = "Creates a new Periods instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Periods createPeriods(@RequestBody Periods periods) {
		LOGGER.debug("Create Periods with information: {}" , periods);

		periods = periodsService.create(periods);
		LOGGER.debug("Created Periods with information: {}" , periods);

	    return periods;
	}

    @ApiOperation(value = "Returns the Periods instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public Periods getPeriods(@PathVariable("id") Integer id) {
        LOGGER.debug("Getting Periods with id: {}" , id);

        Periods foundPeriods = periodsService.getById(id);
        LOGGER.debug("Periods details with id: {}" , foundPeriods);

        return foundPeriods;
    }

    @ApiOperation(value = "Updates the Periods instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Periods editPeriods(@PathVariable("id") Integer id, @RequestBody Periods periods) {
        LOGGER.debug("Editing Periods with id: {}" , periods.getPeriodId());

        periods.setPeriodId(id);
        periods = periodsService.update(periods);
        LOGGER.debug("Periods details with id: {}" , periods);

        return periods;
    }

    @ApiOperation(value = "Deletes the Periods instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
public boolean deletePeriods(@PathVariable("id") Integer id) {
        LOGGER.debug("Deleting Periods with id: {}" , id);

        Periods deletedPeriods = periodsService.delete(id);

        return deletedPeriods != null;
    }

    /**
     * @deprecated Use {@link #findPeriods(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of Periods instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Periods> searchPeriodsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering Periods list by query filter:{}", (Object) queryFilters);
        return periodsService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Periods instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Periods> findPeriods(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Periods list by filter:", query);
        return periodsService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of Periods instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Periods> filterPeriods(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering Periods list by filter", query);
        return periodsService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportPeriods(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return periodsService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of Periods instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countPeriods( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting Periods");
		return periodsService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getPeriodsAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return periodsService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/{id:.+}/incentiveses", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the incentiveses instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<Incentives> findAssociatedIncentiveses(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated incentiveses");
        return periodsService.findAssociatedIncentiveses(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/pyPayoutses", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the pyPayoutses instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<PyPayouts> findAssociatedPyPayoutses(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated pyPayoutses");
        return periodsService.findAssociatedPyPayoutses(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/pyPerformanceses", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the pyPerformanceses instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<PyPerformances> findAssociatedPyPerformanceses(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated pyPerformanceses");
        return periodsService.findAssociatedPyPerformanceses(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service PeriodsService instance
	 */
	protected void setPeriodsService(PeriodsService service) {
		this.periodsService = service;
	}

}

