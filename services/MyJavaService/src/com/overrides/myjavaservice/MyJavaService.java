/*Copyright (c) 2016-2017 All Rights Reserved.
 This software is the confidential and proprietary information of cruiseplanners.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered.*/



package com.overrides.myjavaservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import org.hibernate.Session;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
//import com.wavemaker.runtime.RuntimeAccess;
import org.hibernate.Transaction;
import java.util.*;
import com.overrides.overrides.*;
import com.overrides.overrides.service.*;
import java.text.*;
import java.time.*;
import java.time.format.*;
import java.lang.Math;



@ExposeToClient
public class MyJavaService {

    @Autowired
    private SessionFactory mySessionFactory;
    private Session session;

    //Declare variables
    private static final Logger logger=LoggerFactory.getLogger(MyJavaService.class);
    private static final int year = 2016;

    private int vendor_id; //
    private int period;//
    private int year_selected;//
    private int previous_year;//
    private int days_diff;//

    private String vendor_name; //
    //private String vendor_type; //
    //private String vendor_goal; //
    private int structure_id; //
    private int goal_id;//
    private int type_id;//
   

    private Long py_performance_id;//
    private Long py_payout_id;//
    private Long forecast_id;//

    private double current_rate = 0;
    private int current_level = 0;
    private double actual_amt = 0;

    private LinkedHashMap incentive_tiers = new LinkedHashMap(); //

    //private Date as_of_date = new Date();

    private double sales = 0.0;
    private double qual_sales = 0.0;
    private double units = 0.0;
    private double qual_units = 0.0;
    private double pax = 0.0;
    private double qual_pax = 0.0;

    private double stly_sales = 0.0;
    private double stly_pax = 0.0;
    private double stly_units = 0.0;
    
    private double weight = 1.0;
    private boolean payout_given = false;
    
    private double py_payout_amt = 0;


    //
    // Load the dim values:
    //
    // goals: 1 Sales, 2 Units, 3 PAX
    // level_met: -1 Past, 1 Current, 2 Next, 3 Running
    // levels: 0, 1, 2, etc
    // periods: 1, 2, 3, 4, 5
    // types: 1 Cruise, 2 Land
    // date: as_of_date - 1/1/2016

    //
    
    public String deactivateSummaries(Summaries[] summary_list){
        try {
            SummariesServiceImpl serviceobj = new SummariesServiceImpl();
            Summaries returnedobj;
            
            session = mySessionFactory.openSession();
            Transaction tx = session.beginTransaction();
            String hqlUpdate = "";
            short s3 = 0;
            
            for(Summaries summary : summary_list){
                //summary.setActive(0);
                //System.out.println("summary active: "+summary.getActive());
                //returnedobj = serviceobj.update(summary);
                //Update "active" field with zeros
            hqlUpdate = "update Summaries S set active= :active where summary_id = :summaryid";
            
            System.out.println("starting update query with:");
            System.out.println("summary_id:"+summary.getSummaryId());
            
            int updateEntities = session.createQuery( hqlUpdate )
                    .setString( "summaryid", summary.getSummaryId())
                    .setShort( "active", s3)
                    .executeUpdate();

            System.out.println("update query end");
            }
            tx.commit();    
            return "";
            
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        finally{
            session.close();
        }
    }
    
    
    // public String deactivateSummaries(Integer vendor_id, Integer period, Integer year){
    //     try {
    //         System.out.println("vendor_id: "+vendor_id);
    //         System.out.println("period: "+period);
    //         System.out.println("year: "+year);
            
    //         return "";
    //     } catch (Exception e) {
    //         logger.error("Deactivate summaries operation has failed", e);
    //         e.printStackTrace();
    //         throw e;
    //     }
    // }
    
    

    //Main method
    public String calculateOperation(Date as_of_date, Integer vendor_id, Integer period, Double sales, Double qual_sales, Double units, Double qual_units, Double pax, Double qual_pax, Double stly_sales, Double stly_units, Double stly_pax, String vendor_key, Integer vendor_forecast_type, Double payout, Integer year) {


        String result = null;
        try {
            
            System.out.println("///////////////values from the form begin//////////////////");
            System.out.println("date:"+as_of_date);
            System.out.println("vendor_id:"+vendor_id);
            System.out.println("period:"+period);
            System.out.println("sales:"+sales);
            System.out.println("qual_sales:"+qual_sales);
            System.out.println("units:"+units);
            System.out.println("qual_units:"+qual_units);
            System.out.println("pax:"+pax);
            System.out.println("qual_pax:"+qual_pax);
            System.out.println("stly_sales:"+stly_sales);
            System.out.println("stly_units"+stly_units);
            System.out.println("stly_pax:"+stly_pax);
            System.out.println("vendor_forcast_type:"+vendor_forecast_type);
            System.out.println("payout:"+payout);
            System.out.println("year:"+year);
            System.out.println("///////////////values from the form end//////////////////");
            

            //
            //DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            //System.out.println(df.format(as_of_date));
            //Date start_date = df.parse("01/01/2016 00:00:00");
        
            // System.out.println(as_of_date);
            // System.out.println(as_of_date.getClass());
            // System.out.println(as_of_date.toString());
            // System.out.println(ldt_now);
            // System.out.println(ldt_now.getClass());
            
            //Date util_as_of_date = new SimpleDateFormat("yyyy-MM-dd").parse(as_of_date.toString());
            //System.out.println(util_as_of_date); 
            //System.out.println(util_as_of_date.getClass()); 
            //LocalDate start_date = new LocalDate(2016, 1, 1);
            
            LocalDate ldt_start_date = LocalDate.of(2016, 1, 1);
            LocalDate ldt_now = LocalDate.parse(as_of_date.toString());
            //LocalDate ldt_now = LocalDate.now(ZoneId.of("America/New_York"));
            //as_of_date = new Date(2018,3,27);
            //LocalDate ldt_now = util_as_of_date.toInstant().atZone(ZoneId.of("America/New_York")).toLocalDate();
            
            long days = Duration.between(ldt_start_date.atStartOfDay(),ldt_now.atStartOfDay()).toDays();
            this.days_diff = (int)Math.abs(days);
            
            //this.days_diff = Days.daysBetween(start_date, new LocalDate(as_of_date)).getDays();

            //this.days_diff = (int) daysBetween(start_date, as_of_date);
            //System.out.println("Diff: "+ Integer.toString(days_diff));

            //DateFormat df2 = new SimpleDateFormat("yyyy");
            //this.year_selected = new Integer(df2.format(as_of_date));
            //year = 2016;
            this.year_selected = (int) year;
            this.previous_year = year_selected - 1;
            System.out.println(year_selected);

            this.vendor_id = vendor_id;
            //vendor_id = 6;
            //System.out.println(Integer.toString(vendor_id));

            this.period = period;
            //period = 1;
            //System.out.println(Integer.toString(period));
System.out.println("calculated days_diff:"+days_diff);
            this.sales = qual_sales;
            //sales = 342.00;
            this.qual_sales= qual_sales;
            //qual_sales= 100.00;
            this.units = qual_units;
            //units = 24.00;
            this.qual_units = qual_units;
            //qual_units = 12.00;
            this.pax = qual_pax;
            //pax = 36.00;
            this.qual_pax = qual_pax;
            //qual_pax = 13.00;
            this.stly_sales = stly_sales;
            //stly_sales = 0.14;
            this.stly_pax = stly_pax;
            //stly_pax = 0.14;
            this.stly_units = stly_units;
            //stly_units = 0.14;
            
            //payout = 1.0;
System.out.println("assigned values to variables");

            //DB Connection
            session = mySessionFactory.openSession();
            Transaction tx = session.beginTransaction();
System.out.println("starting delete query with:");
System.out.println("days_diff:"+days_diff);
System.out.println("vendor_id:"+vendor_id);
System.out.println("period:"+period);
System.out.println("year"+year);
            //Delete existing entry with same date
            String hqlDelete = "delete Summaries S where dateId = :date and vendorId = :vendor and periodId = :period and year = :year";
            int deletedEntities = session.createQuery( hqlDelete )
                    .setString( "date", Integer.toString(days_diff))
                    .setString( "vendor", Integer.toString(vendor_id))
                    .setString( "period", Integer.toString(period))
                    .setString( "year", Integer.toString(year))
                    .executeUpdate();
System.out.println("delete query end");
                    
            //Update "active" field with zeros
            String hqlUpdate = "update Summaries S set active= :active where vendorId = :vendor and periodId = :period and year = :year";
            short s3 = 0;
System.out.println("starting update query with:");
System.out.println("days_diff:"+days_diff);
System.out.println("vendor_id:"+vendor_id);
System.out.println("period:"+period);
System.out.println("year"+year);
            int updateEntities = session.createQuery( hqlUpdate )
                    .setString( "vendor", Integer.toString(vendor_id))
                    .setString( "period", Integer.toString(period))
                    .setShort( "active", s3)
                    .setString( "year", Integer.toString(year))
                    .executeUpdate();
System.out.println("update query end");

            //Load py_performance_id
            String hql_1 = "FROM PyPerformances PS WHERE PS.vendorId=" + Integer.toString(vendor_id)+" and PS.yearDt="+Integer.toString(previous_year)+" and PS.periodId="+Integer.toString(period)+" ORDER BY modified DESC";
            Query query_1 = session.createQuery(hql_1);
            query_1.setMaxResults(1);
            List results_1 = query_1.list();

            if(!results_1.isEmpty()) {
                Iterator iterator_1 = results_1.iterator();
                PyPerformances element = (PyPerformances) iterator_1.next();
                py_performance_id = element.getPyPerformanceId();
            }else{
                py_performance_id = new Long(-1);
            } 
System.out.println("calculated py_performance_id: "+py_performance_id);        

            //Load forecastTypes
            //vendor_forecast_type = 1;
            hql_1 = "FROM ForecastTypes FT WHERE FT.forecastTypeId=" + Integer.toString(vendor_forecast_type)+" ORDER BY modified DESC";
            query_1 = session.createQuery(hql_1);
            query_1.setMaxResults(1);
            results_1 = query_1.list();
            
            String selected_forecast_type = "";
            if(!results_1.isEmpty()) {
                Iterator iterator_1 = results_1.iterator();
                ForecastTypes element = (ForecastTypes) iterator_1.next();
                selected_forecast_type = element.getName();
            }else{
                selected_forecast_type = "-1";
            }

System.out.println("Calculated vendor forecast type: "+selected_forecast_type); 

            //Load py_payout_id
            hql_1 = "FROM PyPayouts PP WHERE PP.vendorId=" + Integer.toString(vendor_id)+" and PP.yearDt="+Integer.toString(previous_year)+" and PP.periodId="+Integer.toString(period)+" ORDER BY modified DESC";
            query_1 = session.createQuery(hql_1);
            query_1.setMaxResults(1);
            results_1 = query_1.list();

            if(!results_1.isEmpty()) {
                Iterator iterator_1 = results_1.iterator();
                PyPayouts element = (PyPayouts) iterator_1.next();
                py_payout_id = element.getPyPayoutId();
                py_payout_amt = element.getPayoutAmt();
            }else{
                py_payout_id = new Long(-1);
            }
System.out.println("calculated py_payout_id: "+py_payout_id);
            //Load forecast_id
            // DateFormat df3 = new SimpleDateFormat("yyyy-MM-dd");
            
            // hql_1 = "FROM Forecasts F WHERE F.vendorId=" + vendor_key +" and F.year="+Integer.toString(previous_year)+" and F.quarter="+Integer.toString(period)+" and F.created='"+new String(df3.format(new Date()))+"' and F.metricType='"+selected_forecast_type+"' and (F.periodType='running' or F.periodType='annually') ORDER BY modified DESC";
            // /// quarterly
            // /// created
            // //hql_1 = "FROM Forecasts F WHERE F.vendorId=" + Integer.toString(vendor_id) +" and F.year="+Integer.toString(previous_year)+" and F.periodId="+Integer.toString(period)+" ORDER BY modified DESC";;
            // query_1 = session.createQuery(hql_1);
            // query_1.setMaxResults(1);
            // results_1 = query_1.list();

            // if(!results_1.isEmpty()) {
            //     Iterator iterator_1 = results_1.iterator();
            //     Forecasts element = (Forecasts) iterator_1.next();
            //     forecast_id = element.getForecastId();
            // }else{
                forecast_id = new Long(-1);
            //}
            
System.out.println("calculated forecast_id: "+forecast_id);

            //Load incentive details: vendorId, yearDt, period
            String hql = "FROM Incentives I WHERE I.vendorId=" + Integer.toString(vendor_id)+" and I.yearDt="+Integer.toString(year_selected)+" and I.periodId="+Integer.toString(period)+" ORDER BY tierLevel ASC";
            Query query = session.createQuery(hql);
            List results = query.list();
            //System.out.println(results);    
            Iterator iterator = results.iterator();
            while(iterator.hasNext()){
                Incentives element = (Incentives) iterator.next();

                this.goal_id = element.getGoalId();
                this.type_id = element.getTypeId();
                double incentive_id = element.getIncentiveId();
                
                this.weight = element.getWeight();

                int tier_level = element.getTierLevel().intValue();
                double rate_amt = element.getRateAmt();
                double target_amt = element.getTargetAmt();
                double target_pct = element.getTargetPct();
                double min_amt = rate_amt * target_amt;
                
                // System.out.println(tier_level);
                // System.out.println(rate_amt);
                // System.out.println(target_amt);
                // System.out.println(target_pct);
                System.out.println("cal min amt 1:"+min_amt);
                
                //% to target
                double pct_target = 0;
                double need_build = 0;
                
                //ratio sales/units or PAX
                double ratio = 0.0;
                System.out.println("goal id:"+goal_id);
                
                //Sales
                if (goal_id == 1) {
                    pct_target = qual_sales / target_amt;
                    need_build = target_amt - qual_sales;
                }// Units
                else if (goal_id ==2){
                    ratio = qual_sales / qual_units;
                    pct_target = qual_units / target_amt;
                    need_build = target_amt - qual_units;
                    min_amt = ratio * target_amt * rate_amt;
                }// PAX
                else if (goal_id ==3){
                    //System.out.println("inside if-3");
                    ratio = qual_sales / qual_pax;
                    pct_target = qual_pax / target_amt;
                    need_build = target_amt - qual_pax;
                    //System.out.println("ratio:"+);
                    min_amt = ratio * target_amt * rate_amt;
                }
                System.out.println("cal min amt 2:"+min_amt);

                //current level
                if(need_build <= 0){
                    this.current_level = tier_level;
                    this.current_rate = rate_amt;
                    need_build = -1;
                }
                
                //
                // Actual Amt
                //
                //Sales
                if (goal_id == 1) {
                    this.actual_amt  = qual_sales * current_rate * weight;
                }// Units
                else if (goal_id ==2){
                    this.actual_amt  = (qual_units * ratio) * current_rate * weight;
                }// PAX
                else if (goal_id ==3){
                    this.actual_amt  = (qual_pax * ratio) * current_rate * weight;
                }
                
                if(payout > -1){
                    this.actual_amt = payout;
                    payout_given = true;
                }
                
                
                
                incentive_tiers.put(tier_level, new double[] {rate_amt, target_amt, target_pct, min_amt, pct_target, need_build, incentive_id});

            }//while
            
            Set<Integer> keys1 = incentive_tiers.keySet();
            for (int i = 0; i < keys1.size(); i++){
                double[] temp1 = (double[]) incentive_tiers.get(i + 1);
                System.out.println("calculated rate amt: "+temp1[0]);
                System.out.println("calculated target amt: "+temp1[1]);
                System.out.println("calculated target pct "+temp1[2]);
                System.out.println("calculated min amt: "+temp1[3]);
                System.out.println("calculated pct target: "+temp1[4]);
                System.out.println("calculated need build: "+temp1[5]);
                System.out.println("calculated incentive ID: "+temp1[6]);
            }
            

            //Add to Summaries object
            ArrayList<Summaries> summaries_target = new ArrayList<Summaries>();
            Set<Integer> keys = incentive_tiers.keySet();
            for (int i = 0; i < keys.size(); i++) {

                double[] temp = (double[]) incentive_tiers.get(i + 1);

                UUID idOne = UUID.randomUUID();

                Summaries s = new Summaries();
                s.setSummaryId(String.valueOf(idOne));
                s.setDateId(days_diff);
                s.setGoalId(goal_id);
                s.setVendorId(vendor_id);
                s.setPeriodId(period);
                s.setTypeId(type_id);
                s.setLevelId(i+1);
                s.setIncentiveId((int)temp[6]);

                s.setSales(qual_sales);
                s.setPax(qual_pax);
                s.setUnits(qual_units);
                s.setQualSales(qual_sales);
                s.setQualPax(qual_pax);
                s.setQualUnit(qual_units);
                
                int s1 = 1;
                s.setActive(s1);
                
                s.setYear(year);

                if(py_performance_id != -1){
                    s.setPyPerformanceId(py_performance_id);
                } 
                
                
                if(payout_given){
                    s.setEstPmt(payout);
                    s.setEstPmtSupplier(payout_given);
                }
                
                if(py_payout_id != -1){
                    s.setPyPayoutId(py_payout_id);
                    s.setEstPmt(py_payout_amt);
                    s.setEstPmtSupplier(true);
                }
                
                if(forecast_id != -1){
                    s.setForecastId(forecast_id);
                }else{
                    s.setForecastId(new Long(0));
                }

                s.setStlySalesPct(stly_sales);
                s.setStlyUnitsPct(stly_units);
                s.setStlyPaxPct(stly_pax);

                //s.setIncentivePct(temp[0]);
                //s.setTarget(temp[1]);
                //s.setGrowthPct(temp[2]);
                if (temp[3] != 0){
                    s.setMinPayout(temp[3]);
                    s.setEstPmt(actual_amt);
                    s.setEstPmtSupplier(payout_given);
                }else{
                    s.setMinPayout(null);
                    s.setEstPmt(null);
                    s.setEstPmtSupplier(payout_given);
                }
                
                
                s.setActualPct(temp[4]);
                
                if(temp[5] != -1){
                    s.setBuild(temp[5]);
                }
                
                //formattig the modified date
                // LocalDateTime orig_date = LocalDateTime.now(ZoneId.of("America/New_York"));
                // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                // String text = "";
                // text = orig_date.format(formatter);
                // LocalDateTime parsedDate = LocalDateTime.parse(text, formatter);
                
                // s.setModified(parsedDate);
                //current_level = current_level - 1;
                // level_met: -1 Past, 1 Current, 2 Next, 3 Running
                if (current_level > (i+1)){
                    s.setLevelMetId(-1);
                }else if(current_level == (i+1)){
                    s.setLevelMetId(1);
                }else if ((current_level+1) == (i+1)){
                    s.setLevelMetId(2);
                }else if ((current_level < (i+1))){
                    s.setLevelMetId(3);
                }

                /**
                if(i+2 <= keys.size()){
                    double[] temp2 = (double[]) incentive_tiers.get(i + 2);
                    double min_amt_next_tier = temp2[3];
                    double incremental_amt = min_amt_next_tier - actual_amt;
                    s.setIncremental(incremental_amt);
                }else{
                    s.setIncremental(-1.0);
                }
                 */

                if(qual_sales > 0) {
                    if (temp[5] <= -1) {
                        //s.setIncremental(-1.0);
                    } else {
                        double min_amt = temp[3];
                        double incremental_amt = min_amt - actual_amt;
                        if(incremental_amt == 0){
                            s.setIncremental(null);
                        }else{
                            s.setIncremental(incremental_amt);
                        }
                    }
                }else{
                    s.setIncremental(null);
                }
                
                


                summaries_target.add(s);

            }//for


            
            
            for (Summaries temp_1 : summaries_target) {
                System.out.println(temp_1);
                session.save(temp_1);
            }//for
            
            
            //tx.commit();
            

            
          /**
            //locate the date_id value at the date_link_id (using the days_diff value)
            String hql_2 = "FROM DateLinks D WHERE D.vendorId=" + Integer.toString(vendor_id)+" AND D.dateLinkId="+ Integer.toString(days_diff);
            Query query_2 = session.createQuery(hql_2);
            query_2.setMaxResults(1);
            List results_2 = query_2.list();
            
            int date_id_old = -1;
            
            if(!results_2.isEmpty()) {
                Iterator iterator_2 = results_2.iterator();
                DateLinks element = (DateLinks) iterator_2.next();
                date_id_old = (int) element.getDateId();
            }
          
            
            
            //update all date_id values with the days_diff value using the criteria of date_link_id >= days_diff AND date_id_old == date_id
            String hqlUpdate = "update DateLinks D set D.dateId = :newID WHERE D.vendorId= :vendorID AND D.dateLinkId >= :newID AND D.dateId = :oldID";
            int updatedEntities = session.createQuery( hqlUpdate )
            .setInteger( "newID", days_diff )
            .setInteger( "oldID", date_id_old)
            .setInteger( "vendorID", vendor_id)
            .executeUpdate();
**/

            


            tx.commit();
            
//System.out.println("10");
            //reset actual amount
            this.actual_amt = 0;
            this.current_rate = 0;

            incentive_tiers = new LinkedHashMap();

            //reset current level
            this.current_level =0;

            vendor_id = -1; //
            period = -1;//
            year_selected = -1;//
            days_diff = -1;//

            vendor_name = ""; //
            structure_id = -1; //
            goal_id = -1;//
            type_id = -1;//
            py_performance_id = new Long(-1);//
            py_payout_id = new Long(-1);//
            forecast_id = new Long(-1);//
 

            sales = 0.0;
            qual_sales = 0.0;
            units = 0.0;
            qual_units = 0.0;
            pax = 0.0;
            qual_pax = 0.0;

            stly_sales = 0.0;
            stly_pax = 0.0;
            stly_units = 0.0;
            
            weight = 1.0;
            payout_given = false;
            
            py_payout_amt = 0;

            result = "success";
            return result;


        } catch (Exception e) {
            e.printStackTrace();
            throw new ArithmeticException(e.toString());
            
        }//catch
        finally{
            session.close();
        }
    }//calculateOperation

    public String sampleJavaOperation(String name) {
        String result = null;
        try {
            logger.debug("Starting sample operation");
            result = "Hello " + name + "!";
            logger.debug("Returning {}", result);
            return result;
        } catch (Exception e) {
            logger.error("Sample java service operation has failed", e);
            throw e;
        }
    }

/**
    private static long daysBetween(Date one, Date two) {
        long difference =  (one.getTime()-two.getTime())/86400000;
        return Math.abs(difference);
    }//daysBetween
**/

}//class
